package com.example.hedenclod.tapbot;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    public final static String version = "v0.1 [PoC]";

    private Dialog startDialog;
    private Dialog creditsDialog;

    private static boolean startup = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(!startup) {
            startActivity(new Intent(MainActivity.this, StartupActivity.class));
            startup = true;
            return;
        }
        defineDialogs();
        addClickListeners();
    }

    private void addClickListeners() {
        Button startButton = (Button) findViewById(R.id.startButton);
        Button creditsButton = (Button) findViewById(R.id.creditsButton);
        startButton.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View view) {
                                               startDialog.show();
                                           }
                                       }
        );
        creditsButton.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View view) {
                                                 creditsDialog.show();
                                             }
                                         }
        );
    }

    private void defineDialogs() {
        startDialog = createDialog("TapBot is still unconscious from being tazered", "Ok...", "Clean up evidence");
        creditsDialog = createDialog("Team TapBot: Paul, Menno, Damian and Adam", "Ok", "");

    }

    private Dialog createDialog(String message, String positiveButton, String negativeButton) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);


        builder.setMessage(message).setCancelable(false);
        builder.setNegativeButton(positiveButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        if(!negativeButton.equals("")) {
            builder.setPositiveButton(negativeButton, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
        }
        Dialog dialog = builder.create();
        dialog.setTitle("TapBot alert");
        return dialog;
    }

}
