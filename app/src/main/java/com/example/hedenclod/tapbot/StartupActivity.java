package com.example.hedenclod.tapbot;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import org.w3c.dom.Text;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * Created by hedenclod on 10/8/16.
 */

public class StartupActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    private TextView loadText, versionText;

    private static final String[] loadingText = {"Starting engines", "Trying to wake up TapBot", "Trying harder", "Tazing TapBot"};
    private final static int dotInterval = 600;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);
        progressBar = (ProgressBar) findViewById(R.id.progressBar5);
        loadText = (TextView) findViewById(R.id.loadText);
        versionText = (TextView) findViewById(R.id.versionText);

        versionText.setText(MainActivity.version);
        handleStartup();
    }


    protected void handleStartup() {
        final long startTime = System.currentTimeMillis();

        final AtomicInteger counter = new AtomicInteger();
        final TextView loadTextView = loadText;

        (new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    String oldText = String.valueOf(loadTextView.getText());
                    String newText = loadingText[counter.get()];

                    String dotlessText = "";
                    if (oldText.length() >= newText.length()) {
                        dotlessText = oldText.substring(0, newText.length());
                    }

                    final boolean equals = newText.equals(dotlessText);
                    if (newText.equals(dotlessText)) {
                        if (oldText.endsWith("..")) {
                            int n = counter.incrementAndGet();
                            if (n >= loadingText.length) {
                                exit();
                                break;
                            }
                            newText = loadingText[n];
                        } else {
                            newText = oldText + ".";
                        }
                    }

                    final String finalText = newText;
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            loadText.setText(finalText);
                        }
                    });
                    try {
                        Thread.sleep(dotInterval);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                }
        })).start();
    }

    private void exit() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                postStartup();
            }
        });
    }

    protected void postStartup() {
        startActivity(new Intent(StartupActivity.this, MainActivity.class));
    }

}